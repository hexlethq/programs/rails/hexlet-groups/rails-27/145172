# frozen_string_literal: true
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

statuses = Status.create([{ name: 'New' }, { name: 'In Progress' } , { name: 'Done' }])

users = (1..4).collect { User.create( name: Faker::Name.name ) }

Task.create(name: 'Homework', user: users.first, status: statuses.first)
