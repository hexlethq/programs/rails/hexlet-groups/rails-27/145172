module Web::Movies
  class ReviewsController < ApplicationController
    def index
      @reviews = resource_movie.reviews
    end

    def new
      @review = resource_movie.reviews.build
    end

    def create
      review = resource_movie.reviews.build(review_params)

      if review.save
        redirect_to resource_movie
      else
        render :new, status: :unprocessable_entity
      end
    end

    def edit
      @review = resource_movie.reviews.find(params[:id])
    end

    def update
      review = resource_movie.reviews.find(params[:id])

      if review.update(review_params)
        redirect_to resource_movie
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      review = resource_movie.reviews.find(params[:id])

      if review.destroy
        redirect_to resource_movie
      else
        redirect_to resource_movie, notice: 'Failed to delete review'
      end
    end

    private

    def review_params
      params.require(:review).permit(:body)
    end
  end
end
