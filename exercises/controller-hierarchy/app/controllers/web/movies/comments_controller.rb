module Web::Movies
  class CommentsController < ApplicationController
    def index
      @comments = resource_movie.comments
    end

    def new
      @comment = resource_movie.comments.build
    end

    def create
      comment = resource_movie.comments.build(comment_params)

      if comment.save
        redirect_to resource_movie
      else
        render :new, status: :unprocessable_entity
      end
    end

    def edit
      @comment = resource_movie.comments.find(params[:id])
    end

    def update
      comment = resource_movie.comments.find(params[:id])

      if comment.update(comment_params)
        redirect_to resource_movie
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      comment = resource_movie.comments.find(params[:id])

      if comment.destroy
        redirect_to resource_movie
      else
        redirect_to resource_movie, notice: 'Failed to delete comment'
      end
    end

    private

    def comment_params
      params.require(:comment).permit(:body)
    end
  end
end
