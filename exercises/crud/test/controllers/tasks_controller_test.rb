require "test_helper"

class TasksControllerTest < ActionDispatch::IntegrationTest
  self.use_transactional_tests = true

  test 'index action' do
    get tasks_path
    assert_response :success
  end

  test 'new action' do
    get new_task_path
    assert_response :success
  end

  test 'create action when occurs successfuly created form' do
    post tasks_path, params: { 
      task: {
        name: 'Task_1',
        description: 'MyText',
        status: 'MyString',
        creator: 'MyString',
        performer: 'MyString',
        completed: false
      }
    }

    assert_redirected_to task_path(Task.order(:created_at).last)
  end

  test 'create action when recieved invalid data' do
    post tasks_path, params: { 
      task: {
        name: 'Task_1',
        description: 'MyText',
        status: 'MyString'
      }
    }

    assert_response 422
  end

  test 'show action' do
    get task_path(tasks(:task_one))
    assert_response :success
  end

  test 'edit action' do
    get edit_task_path(tasks(:task_one))
    assert_response :success
  end

  test 'update action when occurs successfuly updated form' do
    put task_path(tasks(:task_one)), params: { 
      task: {
        name: 'Task_1',
        description: 'MyText',
        status: 'MyString',
        creator: 'MyString',
        performer: 'MyString',
        completed: true
      }
    }
    assert_redirected_to task_path(tasks(:task_one))
  end
  
  test 'update action when received invalid data' do
    put task_path(tasks(:task_one)), params: { 
      task: {
        name: '',
        description: 'MyText',
        status: 'MyString',
        creator: 'MyString',
        performer: 'MyString',
        completed: true
      }
    }
    assert_response 422
  end

  test 'destroy action on a successful case' do
    delete task_path(tasks(:task_one))

    assert_redirected_to(tasks_path)
    assert_equal 'Task deleted successfully', flash[:notice]
  end

  test 'destroy action on a unsuccessful case' do
    unexist_id = 234
    delete task_path(unexist_id)

    assert_redirected_to(tasks_path)
    assert_equal 'Task was already deleted', flash[:notice]
  end
end
