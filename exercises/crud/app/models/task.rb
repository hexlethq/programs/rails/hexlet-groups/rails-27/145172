# == Schema Information
#
# Table name: tasks
#
#  id          :integer          not null, primary key
#  completed   :boolean
#  creator     :string
#  description :text
#  name        :string
#  performer   :string
#  status      :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Task < ApplicationRecord
  attribute :status, default: -> { 'new' }
  attribute :completed, default: -> { false }
  attribute :performer, default: -> { 'unassign' }
  
  validates :name, :creator, :status, presence: true
end
