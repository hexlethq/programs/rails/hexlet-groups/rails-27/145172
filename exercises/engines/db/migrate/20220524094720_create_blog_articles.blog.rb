# This migration comes from blog (originally 20220524093706)
class CreateBlogArticles < ActiveRecord::Migration[6.1]
  def change
    create_table :blog_articles do |t|
      t.string :title
      t.text :text

      t.timestamps
    end
  end
end
