# frozen_string_literal: true

# BEGIN
def fizz_buzz(start, stop)
  return nil if start > stop

  (start..stop).to_a.map do |n|
    case 0
    when n % 15 then 'FizzBuzz'
    when n % 3 then 'Fizz'
    when n % 5 then 'Buzz'
    else n
    end
  end.join(' ')
end
# END
