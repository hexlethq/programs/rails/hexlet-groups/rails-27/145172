# frozen_string_literal: true

# BEGIN
def fibonacci(index)
  return nil if index <= 0

  return index - 1 if index <= 2

  fibonacci(index - 1) + fibonacci(index - 2)
end
# END
