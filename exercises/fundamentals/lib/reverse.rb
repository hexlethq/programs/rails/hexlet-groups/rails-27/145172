# frozen_string_literal: true

# BEGIN
def reverse(str)
  count = str.size - 1
  acc = []
  while 0 <= count
    acc.push str[count]
    count -= 1
  end
  acc.join
end
# END
