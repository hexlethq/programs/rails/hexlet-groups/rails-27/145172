# frozen_string_literal: true

# BEGIN
def count_by_years(users)
  users.each_with_object({}).each do |item, hash|
    next if item[:gender] == 'female'

    year, = item[:birthday].split('-')
    hash[year] ||= 0
    hash[year] += 1
  end
end
# END
