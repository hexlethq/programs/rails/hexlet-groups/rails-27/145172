# frozen_string_literal: true

# BEGIN
def get_same_parity(list)
  return [] if list.empty?

  first_even = list.first.even?
  list.select { |num| first_even ? num.even? : num.odd? }
end
# END
