# frozen_string_literal: true

require 'open-uri'

class Hacker
  class << self
    def hack(email, password)
      # BEGIN
      domain = 'https://rails-l4-collective-blog.herokuapp.com/users'
      unparsed_doc = URI.open("#{domain}/sign_up")
      cookie = unparsed_doc.meta["set-cookie"]
      html_document = Nokogiri::HTML(unparsed_doc)
      token = html_document.css('input[name="authenticity_token"]').first.attributes['value'].value
      uri = URI(domain)
      Net::HTTP.start(uri.host, uri.port, :use_ssl => true) do |http|
        request = Net::HTTP::Post.new uri
        request["Cookie"] = cookie
        request.form_data = {
          'authenticity_token': token,
          "user[email]": email,
          "user[password]": password,
          "user[confirmation]": password,
          'commit': 'Регистрация'}
        response = http.request request
      end
    end
    # END
  end
end
