# frozen_string_literal: true

# rubocop:disable Style/For
# BEGIN
def build_query_string(obj)
  sorted_keys = obj.keys.sort
  result = sorted_keys.each_with_object('').map do |item|
    "#{item}=#{obj[item]}"
  end
  result.join('&')
end
# END
# rubocop:enable Style/For
