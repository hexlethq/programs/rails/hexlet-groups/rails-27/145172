# frozen_string_literal: true

# rubocop:disable Style/For

def make_censored(text, stop_words)
  # BEGIN
  result = text.split.map do |item|
    (stop_words.include? item) ? '$#%!' : item
  end
  result.join(' ')
  # END
end

# rubocop:enable Style/For
