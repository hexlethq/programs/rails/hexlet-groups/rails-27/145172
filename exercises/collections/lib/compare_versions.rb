# frozen_string_literal: true

# BEGIN
def compare_versions(version1, version2)
  return 0 if version1 == version2

  first_version = version1.split('.')
  second_version = version2.split('.')

  compare_major = first_version.first.to_i <=> second_version.first.to_i
  compare_minor = first_version.last.to_i <=> second_version.last.to_i
  
  compare_major.zero? ? compare_minor : compare_major
end
# END
