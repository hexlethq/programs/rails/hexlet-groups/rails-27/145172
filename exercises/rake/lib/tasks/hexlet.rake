require 'csv'

namespace :hexlet do
  desc "Load users from CSV file"
  task :import_users, [:path] => [:environment] do |t, args|
    path = Pathname.new(args[:path])
    path_to_file = path.absolute? ? path : Rails.root.join(path)

    puts "#{ '*'*10 } Start creating users... #{ '*'*10 }"

    CSV.foreach(path_to_file, headers: true) do |row|
      User.create!(row.to_h)
    end

    puts "#{ '*'*10 } Users was created successfully #{ '*'*10 }"
  end
end
