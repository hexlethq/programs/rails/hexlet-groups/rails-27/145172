# frozen_string_literal: true

Rails.application.routes.draw do
  root 'homes#index'

  # BEGIN
  resources :posts do
    scope module: 'posts', shallow: true do
      resources :comments, except: [:new, :show]
    end
  end
  # END
end
