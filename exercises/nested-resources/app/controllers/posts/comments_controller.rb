# frozen_string_literal: true

module Posts
  class CommentsController < ApplicationController
    def create
      @comment = PostComment.new do |u|
        u.post_id = params[:post_id]
        u.body = comment_params[:body]
      end
      
      if @comment.save
        redirect_to post_path(params[:post_id]), notice: 'Comment created successfully'
      else
        @post = Post.find(params[:post_id])
        render 'posts/show', alert: 'Comment not created'
      end
    end

    def edit
      @comment = PostComment.find(params[:id])
    end

    def update
      @comment = PostComment.find(params[:id])
      if @comment.update(comment_params)
        redirect_to post_path(@comment.post_id), notice: 'Post updated'
      else
        render :edit, status: :unprocessable_entity, notice: 'Post not updated'
      end
    end

    def destroy
      comment = PostComment.find(params[:id])
      comment.destroy

      redirect_to post_path(comment.post), notice: 'Comment deleted'
    end

    private

    def comment_params
      params.require(:post_comment).permit(:body)
    end
  end
end
