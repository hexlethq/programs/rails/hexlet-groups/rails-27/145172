# frozen_string_literal: true

class CommentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @post = posts(:one)
    @comment = post_comments(:one)
    @attrs = {
      body: @comment.body,
      post_id: @post.id
    }
  end

  test 'should create comment' do
    post post_comments_url(@post, params: { post_comment: @attrs })
    assert_redirected_to post_url(@post)
  end

  test 'should get edit' do
    get edit_comment_url(@comment, params: { post_id: @post.id })
    assert_response :success
  end

  test 'should update comment' do
    patch comment_url(@comment, params: { post_comment: @attrs })

    @comment.reload

    assert { @comment.body == @attrs[:body] }
    assert_redirected_to post_url(@post)
  end

  test 'should delete comment' do
    delete comment_path(@comment, post_id: @post.id)

    assert { !PostComment.exists?(@comment.id) }
    assert_redirected_to post_url(@post)
  end
end
