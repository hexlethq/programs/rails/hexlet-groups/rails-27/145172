# frozen_string_literal: true

class FetchRepositoryService
  def call(url)
    repo_raw = Octokit::Repository.from_url(url)
    repo = Octokit::Client.new.repo(repo_raw)
    repo_attrs = {
      link: repo[:html_url],
      owner_name: repo[:owner][:login],
      repo_name: repo[:name],
      description: repo[:description],
      default_branch: repo[:default_branch],
      watchers_count: repo[:watchers_count],
      language: repo[:language],
      repo_created_at: repo[:created_at],
      repo_updated_at: repo[:updated_at]
    }
  end
end
