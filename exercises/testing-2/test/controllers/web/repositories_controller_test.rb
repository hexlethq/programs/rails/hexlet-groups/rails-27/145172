# frozen_string_literal: true

require 'test_helper'

class Web::RepositoriesControllerTest < ActionDispatch::IntegrationTest
  # BEGIN
  setup do
    @response = load_fixture('files/response.json')
    parsed_response = JSON.parse(@response)
    @owner = parsed_response['owner']['login']
    @repo_name = parsed_response['name']
    repo_url = "https://api.github.com/repos/#{@owner}/#{@repo_name}"
    @repo_params = { link: repo_url }
  end

  test 'should create' do
    stub_request(:get, "https://api.github.com/repos/#{@owner}/#{@repo_name}")
      .to_return(body: @response, status: 200, headers: { content_type: 'application/json' })

    post repositories_url, params: { repository: @repo_params }
    assert { Repository.exists?(owner_name: @owner, repo_name: @repo_name) }
    assert_redirected_to repositories_url
  end
  # END
end
