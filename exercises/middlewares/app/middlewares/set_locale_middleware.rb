# frozen_string_literal: true

class SetLocaleMiddleware
  # BEGIN
  def initialize(app)
    @app = app    
  end

  def call(env)
    dup._call(env)
  end

  def _call(env)
    if env['HTTP_ACCEPT_LANGUAGE']
      locale = env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
      locale ||= I18n.default_locale
      I18n.locale = locale
    end

    @app.call(env)
  end
  # END
end
