# frozen_string_literal: true

require 'rack'

module App
  # show execution time
  class ExecutionTimer
    def initialize(app)
      @app = app
    end

    def call(env)
      runtime = Rack::Runtime.new(@app)
      runtime.call(env)
    end
  end
end
