# frozen_string_literal: true

require 'rack'

module App
  # filter requests
  class AdminPolicy
    def initialize(app)
      @app = app
    end

    def call(env)
      req = Rack::Request.new(env)
      if req.path.match(%r{^/admin})
        [403, {}, '']
      else
        @app.call(env)
      end
    end
  end
end
