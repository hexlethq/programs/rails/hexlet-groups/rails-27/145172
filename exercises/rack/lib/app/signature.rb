# frozen_string_literal: true

require 'rack'
require 'digest'

module App
  # add hash to body of response
  class Signature
    def initialize(app)
      @app = app
    end

    def call(env)
      status, headers, body = @app.call(env)
      if status == 200
        new_body = "#{body}\n#{Digest::SHA256.hexdigest(body)}"
        [status, headers, new_body]
      else
        [status, headers, body]
      end
    end
  end
end
