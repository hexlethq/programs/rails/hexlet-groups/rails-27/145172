# frozen_string_literal: true

class PostPolicy < ApplicationPolicy
  # BEGIN
  def new?
    create?
  end

  def create?
    user
  end

  def edit?
    update?
  end

  def update?
    user&.admin? or record.author_id == user&.id
  end

  def destroy?
    user&.admin?
  end
  # END
end
