# frozen_string_literal: true
require 'uri'
require 'forwardable'

# BEGIN
class Url
  include URI
  include Comparable
  extend Forwardable
  
  attr_reader :url
  def initialize(url_string)
    @url = URI(url_string)
  end

  def_delegators :@url, :scheme, :host
  
  def query_params
    url.query.split('&').each_with_object({}).each do |item, acc|
      pair = item.split('=')
      acc[pair.first.to_sym] = pair.last
    end
  end

  def query_param(key, second = nil)
    if self.query_params.has_key?(key)
      return self.query_params[key]
    else
      second
    end
  end

  def <=>(other)
    url.to_s <=> other.url.to_s
  end

end
# END
