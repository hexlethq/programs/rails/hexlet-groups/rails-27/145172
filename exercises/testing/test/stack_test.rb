# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def test_that_stack_delete_last_item
    stack = Stack.new ['github', 'gitlab', 'bitbucket']
    result = stack.pop!

    assert(result == 'bitbucket')
    assert(stack.to_a == ['github', 'gitlab'])
  end

  def test_when_pop_method_called_empty_stack
    stack = Stack.new []
    result = stack.pop!

    assert_nil(result)
  end

  def test_that_stack_add_item
    stack = Stack.new [1]
    result = stack.push! 2

    assert(result == [1, 2])
  end

  def test_that_stack_is_empty
    stack = Stack.new ['something']
    stack.pop!

    assert(stack.empty?)
  end

  def test_that_stack_return_array
    stack = Stack.new ['github', 'gitlab', 'bitbucket']

    assert(stack.to_a == ['github', 'gitlab', 'bitbucket'])
  end

  def test_that_stack_cleared
    stack = Stack.new ['something']
    stack.clear!

    assert(stack.empty?)
  end

  def test_that_shows_size_stack
    stack = Stack.new ['github', 'gitlab', 'bitbucket']

    assert(stack.size == 3)
  end
  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
