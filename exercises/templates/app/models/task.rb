class Task < ApplicationRecord
  attribute :status, default: -> { 'new' }
  validates :name, :creator, :status, presence: true
end
