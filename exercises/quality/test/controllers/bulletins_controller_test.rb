# frozen_string_literal: true
require 'test_helper'

class BulletinsControllerTest < ActionDispatch::IntegrationTest
  self.use_transactional_tests = true

  test "should get bulletins" do
    get bulletins_path

    assert_response :success
    assert_select "h1", "Bulletins"
  end

  test "should show certain bulletin" do
    get bulletin_path(bulletins(:published_bulletin))

    assert_response :success
    assert_select "h1", "Title 1"
    assert_select "p", "Description 1"
  end

  test "shouldn't show unpublished bulletin" do
    get bulletin_path(bulletins(:unpublished_bulletin))

    assert_response :success
    assert_select 'mark', 'the bulletin is unpublished'
  end
end
