# frozen_string_literal: true

class Repository < ApplicationRecord
  validates :link, presence: true, uniqueness: true

  # BEGIN
  include AASM

  aasm do
    state :created, initial: true
    state :fetching
    state :fetched
    state :failed

    event :start_fetch do
      transitions from: %i[created fetched], to: :fetching
    end

    event :finish_fetch do
      transitions from: :fetching, to: :fetched 
    end

    event :rescue_fetch do
      transitions from: :fetching, to: :failed
    end
  end
  # END
end
