# frozen_string_literal = true

class RepositoryLoaderJob < ApplicationJob
  def perform(repo_id)
    repository = Repository.find(repo_id)
    parsed_repo_url = Octokit::Repository.from_url(repository.link)

    repository.start_fetch!
    repo_info = Octokit::Client.new.repository(parsed_repo_url)
    repo_params = {
      link: repo_info.html_url,
      owner_name: repo_info.owner_name,
      repo_name: repo_info.repo_name,
      description: repo_info.description,
      default_branch: repo_info.default_branch,
      watchers_count: repo_info.watchers_count,
      language: repo_info.language,
      repo_created_at: repo_info.created_at,
      repo_updated_at: repo_info.updated_at
    }
    repository.update!(repo_params)
    repository.finish_fetch!
  rescue
    repository.rescue_fetch!
  end
end
