# frozen_string_literal: true

require 'application_system_test_case'

# BEGIN
class PostsTest < ApplicationSystemTestCase
  setup do
    @post = posts(:two)
    @post_args = {
      title: 'Title',
      body: 'Body'
    }
    @comment_args = { body: 'Body' }
  end

  test 'viewing the index' do
    visit posts_url
    assert_selector "h1", text: 'Posts'
  end

  test 'creating a Post' do
    visit posts_url
    click_on 'New Post'

    fill_in 'Title', with: @post_args[:title]
    fill_in 'Body', with: @post_args[:body]
    click_on 'Create Post'

    assert { Post.exists? @post_args }
    assert_text 'Post was successfully created'
    click_on 'Back'
  end

  test 'show a Post' do
    visit posts_url
    click_on 'Show', match: :first

    assert_selector 'h1', text: @post.title
    click_on 'Back'
  end

  test 'creating a comment to Post' do
    visit posts_url
    click_on 'Show', match: :first

    fill_in 'post_comment[body]', with: @comment_args[:body]
    click_on 'Create Comment'

    assert { Post::Comment.exists? @comment_args }
    assert_text 'Comment was successfully created'
    click_on 'Back'
  end

  test 'updating a Post' do
    visit posts_url
    click_on 'Edit', match: :first

    fill_in 'Title', with: @post_args[:title]
    fill_in 'Body', with: @post_args[:body]
    click_on 'Update Post'
    
    assert { Post.exists? @post_args }
    assert_text 'Post was successfully updated'
    click_on 'Back'
  end

  test 'destroying a Post' do
    visit posts_url
    page.accept_confirm do
      click_on 'Destroy', match: :first
    end

    assert_text 'Post was successfully destroyed'
  end
end
# END
