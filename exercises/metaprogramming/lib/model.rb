# frozen_string_literal: true

# BEGIN
module Model
  def self.included(base)
    base.extend(ClassMethods)
  end

  def initialize(parameters = {})
    @attributes = {}
    self.class.schema.each do |name, options|
      @attributes[name] = parameters.key?(name) ? convert_type(parameters[name], options[:type]) : nil
    end
  end

  def attributes
    @attributes    
  end

  def convert_type(value, type)
    case type
    when :integer
      value.to_i
    when :string
      value.to_s
    when :datetime
      DateTime.parse value
    when :boolean
      !!value
    end
  end

  module ClassMethods
    attr_reader :schema
    def attribute(name, options)
      @schema ||= {}
      @schema[name] = options
      
      # Getter
      define_method name do
        @attributes[name]
      end

      # Setter
      define_method "#{name}=" do |value|
        @attributes[name] = convert_type(value, options[:type])
      end
    end
  end
end
# END
